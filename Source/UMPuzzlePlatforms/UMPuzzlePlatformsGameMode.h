// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UMPuzzlePlatformsGameMode.generated.h"

UCLASS(minimalapi)
class AUMPuzzlePlatformsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AUMPuzzlePlatformsGameMode();
};



