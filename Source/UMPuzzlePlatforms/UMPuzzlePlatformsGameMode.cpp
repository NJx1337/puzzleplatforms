// Copyright Epic Games, Inc. All Rights Reserved.

#include "UMPuzzlePlatformsGameMode.h"
#include "UMPuzzlePlatformsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUMPuzzlePlatformsGameMode::AUMPuzzlePlatformsGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
