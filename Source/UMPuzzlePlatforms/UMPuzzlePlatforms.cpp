// Copyright Epic Games, Inc. All Rights Reserved.

#include "UMPuzzlePlatforms.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UMPuzzlePlatforms, "UMPuzzlePlatforms" );
 